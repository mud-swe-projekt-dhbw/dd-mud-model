@startuml Klassendiagramm Login-Service
hide footbox

title Login Service\n

left to right direction
package "LoginService"
{
package boundary {
    package "model" {
        
        class "CredentialsTO" as credentialsTO {
            - mail : String
            - username : String
            - password : String
        }

        class "ErrorMessage" as errorMessage {
            - error : String
        }

        class "Mail" as mail {
            - senderAddress : String
            - senderName : String
            - receiverAddress : String
            - subject : String
            - message : String
        }

        class "PasswordForgottenTO" as passwordForgottenTO {
            - mailHash : String
            - password : String
        }

        class "PasswordRequestTO" as passwordRequestTO {
            - mail : String
        }

        class "PlayerInformationTO" as PlayerInformationTO {
            - playerId : Long
            - rank : Long
        }

        class "SignInTO" as signInTO {
            - mailOrUsername : String
            - password : String
        }
    }

    class "LoginApi" as api <<Rest-Controller>> {
        - hashingService : HashingService
    }
}

package control {

    package "aggregator" {

        class "PasswordForgottenAggregator" as pwForgotten <<Service>>{
            - mailService : MailService
            - loginRepositoryService : LoginRepositoryService
            - hashingService : HashingService

            + sendPasswordForgottenMail(mailAdress : String)
            + resetPassword(mailHash : String, password : String)
        }

        class "NewCredentialsAggregator" as ncAggregator <<Service>> {
            - mailService : MailService
            - loginRepositoryService : LoginRepositoryService
            - hashingService : HashingService

            + createNewCredentials(credentialsTO : CredentialsTO)
            + confirmRegistration(mailHash : String)
            - validateMail(mail : String) : boolean
        }

        class "CheckCredentialsAggregator" as ccAggregator <<Service>>{
            - credentialsVerifierService : CredentialsVerifierService
            - loginRepositoryService : LoginRepositoryService
            - passwordCheckingService : PasswordCheckingService

            + checkCredentials(signInTO : SignInTO) : PlayerInformationTO
        }
    }

    package "services" {

        class "CredentialsVerifierService" as cvService <<Service>>{
            - loginRepositoryService : LoginRepositoryService
            - mailService : MailService

            + getUser(signInTO : SignInTO) : User
            + isMail(signInTO : SignInTO) : boolean
            + verifyRegistration(user : User)
        }

        class "HashingService" as hashingService <<Service>>{
            + hash(password : String) : String
            - bytesToHex(hash : byte[]) : String
        }

        class "LoginRepositoryService" as lrService <<Service>>{
            - usernamePasswordRepository : CredentialsRepository

            + saveCredentials(username : String, passwordHash : String, mail : String, mailHash : String)
            + getUserFromUsername(username : String) : User
            + usernameExists(username : String) : boolean
            + mailExists(mail : String) boolean
            + getRegistrationStatusByMail(mail : String) : boolean
            + setRegistrationStatusByMailHash(mailHash : String)
            + getUserByMail(mail : String) : User
            + getUserByMailHash(mailHash : String) : User
            + saveUser(user : User)
        }

        class "MailService" as mailService <<Service>>{
            # mailSession : Session

            + isMailValid(mail : String) : boolean
            + loginToMailServer()
            + sendMail(mail : Mail)
            + logoutFromMailServer()
            + buildConfirmationMail(receiverAddres : String, mailHash : String) : Mail
            + buildPasswordForgottenMail(receiverAddres : String, mailHash : String) : Mail
            - configureMailServer() : Properties
        }

        class "PasswordCheckingService" as pcService <<Service>>{
            - hashingService : HashingService
            - loginRepositoryService : LoginRepositoryService

            + checkPasswordAndReturnInformationTO(credentialsTO : CredentialsTO) : PlayerInformationTO
            + checkPasswordAndReturnInformationTO(user : User, enteredPassword : String) : PlayerInformationTO
            - isHashEqual(usernamePasswordHash : String, enteredPassword : String) : boolean
        }
    }

    package "exceptions" {

        class "EmailNotValidException" as emailNotValidException {

        }

        class "PasswordIsWrongException" as passwordIsWrongException {

        } 

        class "RegistrationNotCompletedException" as registrationNotCompletedException {

        }

        class "UsernameAlreadyExistsException" as usernameAlreadyExistsException {

        } 

        class "UsernameOrMailDoesNotExistException" as usernameOrMailDoesNotExistException {

        }
    }

    class "FrbidenSymbolsUsername" as forbiden
    {
        + FORBIDDEN_AT : String
        + FORBIDDEN_WHITESPACE : String
        + FORBIDDEN_COLON : String
        + FORBIDDEN_USERNAME : String
    }
}

package entity {

    package "modell" {

        class "User" as user {
            - Id : Long
            - username : String
            - mailHash : String
            - mail : String
            - passwordHash : String
            - registrationStatus : boolean
            - rank : Long
        }
    }

    package "repository" {

        interface "CredentialsRepository" as CR {
            + findByUsername(username : String) : Optional<User>
            + findByMail(mail : String) : Optional<User>
            + findByMailHash(mailHash : String) : Optional<User>
            + existsByUsername(username : String) : boolean
            + existsByMail(mail : String) : boolean
        }
    }
}
}

LoginApi -> ccAggregator
LoginApi -> ncAggregator
LoginApi -> PasswordForgottenAggregator

@enduml